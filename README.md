## GitLab Flow et revue de code

1. Pourquoi ne travaille-t-on pas uniquement sur la branche main ?
> On ne travaille pas que sur la branche main pour éviter que **tout soit constamment en conflit**, notamment lorsque plusieurs développeurs travaillent ensemble sur le même projet mais pas la même fonctionnalité.

2. Pourquoi ne doit-on pas créer une seule branche pour chaque développeur·se ?
> Une branche correspond à une fonctionnalité et non à un développeur. Son nom doit être représentatif de la fonctionnalité travaillé. 

3. Qu'est-ce qu'un workflow ?
> Un workflow est une méthode de travail. C'est une organistaion du flux du travail. 

4. Dans quel cas doit-on créer une nouvelle branche avec le GitLab flow ?
> On doit créer une nouvelle branche lorsqu'on doit créer une ou des nouvelles fonctionnalités pendant un sprint.

5. Qu'est-ce qu'une demande de fusion ?
> Une demande de fusion permet de push sa nouvelle branche de fonctionnalités et de demander un code review avant de fusionner avec la branche principale.  

6. Qu'est-ce qu'une revue de code ?
> Une revue de code permet aux développeurs de faire relire leur code avant de la fusion avec la branche principale. Ca leur permet de vérifier que les changements à apporter sont cohérents et aussi de demander de l'aide si besoin.